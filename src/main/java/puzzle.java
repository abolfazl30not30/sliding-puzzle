import javax.swing.text.ChangedCharSetException;
import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class puzzle {
    public static void main(String[] args) {
        int [][] puzzleArray = new int[4][4];
        String [][] charArray = new String[12][4];

        // the position of chosen cell
        int [] position = new int[2];
        position[0] = 0;
        position[1] = 0;

        //Start the game
        Start(charArray,puzzleArray,position);

       while (!checkPuzzleArray(puzzleArray)){
           Choose(puzzleArray,charArray,position);
           Change(puzzleArray,position);
           convertArrays(charArray,puzzleArray,position);
           print(charArray);
       }

        System.out.println("You Win!!!!!");
    }

    /**
     *for staring the game for first time
     * @param puzzleArray
     */
    static void Start(String charArray[][],int puzzleArray[][],int position[]){
        initializedPuzzleArray(puzzleArray);
        shufflePuzzleArray(puzzleArray);
        initializedCharArray(charArray);
        convertArrays(charArray,puzzleArray,position);
        print(charArray);
    }

    /**
     * initialized the index of puzzleArray from 0 to 15
     * @param puzzleArray
     */

    static void initializedPuzzleArray(int puzzleArray[][]){
        int count = 0;
        for (int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++){
                puzzleArray[i][j] = count;
                count++;
            }
        }
    }

    /**
     * shuffle the index of puzzleArray
     * @param puzzleArray
     */

    static void shufflePuzzleArray(int puzzleArray[][]){

        Random rgen = new Random();
        for (int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++){
                int row = rgen.nextInt(3);
                int col = rgen.nextInt(3);
                int temp = puzzleArray[row][col];
                puzzleArray[row][col] = puzzleArray[i][j];
                puzzleArray[i][j] = temp;
            }
        }
    }

    /**
     * initializing String Array of table with space
     * @param charArray
     */

    static void initializedCharArray(String charArray[][]){
        for(int i = 0; i < 12 ; i++){
            for(int j = 0; j < 4; j++){
                charArray[i][j] = "        ";
            }
        }
    }

    /**
     * convert puzzle Array to String Array
     * @param charArray
     * @param puzzleArray
     * @param position
     */
    static void convertArrays(String charArray[][],int puzzleArray[][],int position[]){

        initializedCharArray(charArray);

        for(int i = 0; i < 4 ; i++){
            for(int j = 0; j < 4; j++){
                if(position[0] == i && position[1] == j){
                    String temp = String.format("%2d",puzzleArray[i][j]);
                    if(puzzleArray[i][j] == 0){
                        temp = "  ";
                    }
                    charArray[i * 3 + 0][j] = " \u2554\u2550\u2550\u2550\u2550\u2557 ";
                    charArray[i * 3 + 1][j] = " \u2551"+temp+"  \u2551 ";
                    charArray[i * 3 + 2][j] = " \u255A\u2550\u2550\u2550\u2550\u255D ";
                }
                else {
                    String temp = String.format("%4d", puzzleArray[i][j]);
                    if (puzzleArray [i][j] == 0){
                        temp = "    ";
                    }
                    charArray[i * 3 + 1][j] = temp + "    ";
                }
            }
        }
    }

    /**
     * Printing the table in the command line
     * @param charArray
     */
    static void print (String charArray[][]){
        int counter = 0;

        System.out.println();
        for(int i = 0; i < 17; i++){
            //first row
            if(i == 0){
                for(int j = 0; j < 4; j++){
                    if(j == 0){
                        System.out.print("\u2554");
                        System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550");
                    }
                    else if(j > 0 && j < 4 - 1){
                        System.out.print("\u2566");
                        System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550") ;
                    }
                    else{
                        System.out.print("\u2566");
                        System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550");
                        System.out.print("\u2557");
                    }
                }
                System.out.println();
            }
            //next row
            else if(i > 0 && i < 16){
                //space
                if(i % 4 != 0){
                    for(int j = 0; j < 4; j++){
                        if(j < 4 - 1){
                            System.out.print("\u2551");
                            System.out.print(charArray[counter][j]);
                        }
                        else{
                            System.out.print("\u2551");
                            System.out.print(charArray[counter][j]);
                            System.out.print("\u2551");
                        }
                    }
                    counter++;
                }
                //graphic
                else if (i % 4 == 0){
                    for(int j = 0; j < 4; j++){
                        if(j == 0){
                            System.out.print("\u2560");
                            System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550");
                        }
                        else if(j > 0 && j < 4 - 1){
                            System.out.print("\u256c");
                            System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550");
                        }
                        else{
                            System.out.print("\u256c");
                            System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550");
                            System.out.print("\u2563");
                        }
                    }
                }
                System.out.println();
            }
            else{
                for(int j = 0; j < 4; j++){
                    if(j == 0){
                        System.out.print("\u255A");
                        System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550");
                    }
                    else if(j > 0 && j < 4 - 1){
                        System.out.print("\u2569");
                        System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550");;
                    }
                    else{
                        System.out.print("\u2569");
                        System.out.print("\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550" + "\u2550");
                        System.out.print("\u255D");
                    }
                }
            }
        }//end of main for
        System.out.print("\n\n"+"---------------------------------------------------------------------------------------------------------------------");
        System.out.print("\n\t\t   UP = W             RIGHT = D            change   = C            ");
        System.out.print("\n\t\t   DOWN = S           LEFT = A                                             ");
        System.out.println();
        System.out.println();
    }

    /**
     * checking the puzzle Array are sorted or not
     * @param puzzleArray
     * @return
     */
    static boolean checkPuzzleArray(int puzzleArray[][]){
        int counter = 1;
        for (int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++){
                if(puzzleArray[i][j] != counter){
                    break;
                }
                counter++;
            }
        }


        if(counter == 16){
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * choosing the cell of the table
     * @param puzzleArray
     * @param charArray
     * @param position
     */

    static void Choose (int puzzleArray [][],String charArray[][],int position []){
        Scanner in = new Scanner(System.in);
        String error;
        boolean validation = false;
        char key;
        while (true){

            error ="";

            System.out.print("Enter a key :");
            key = in.next().charAt(0);

            //input validation
            while(!(key == 'w' || key == 'a' || key == 's' || key == 'd' || key == 'c')){
                System.out.println("Invalid input, please try again :");
                key = in.next().charAt(0);
            }

            switch (key){
                case 'w':
                    if(position[0] != 0){
                        position[0]--;
                    }
                    else {
                        position[0] = 3;
                    }
                    break;
                case 's':
                    if (position[0] != 3){
                        position[0]++;
                    }
                    else {
                        position[0] = 0;
                    }
                    break;
                case 'a':
                    if(position[1] != 0){
                        position[1]--;
                    }
                    else {
                        position[1] = 3;
                    }
                    break;
                case 'd':
                    if (position[1] != 3){
                        position[1]++;
                    }
                    else {
                        position[1] = 0;
                    }
                    break;
                case 'c':
                    if(isValid(puzzleArray,position) == true){
                        error = "";
                        validation = true;
                    }
                    else{
                        error = "Please choose valid cell";
                        validation = false;
                    }
                    break;
            }

            convertArrays(charArray,puzzleArray,position);
            print(charArray);
            System.out.println(error);

            //if the user choose correct cell break the while
            if(validation){
                break;
            }
        }
    }

    /**
     * checking the cell that selected is valid or not
     * @param puzzleArray
     * @param position
     * @return
     */
    static boolean isValid (int puzzleArray [][],int position[]){
        int row = position[0];
        int col = position[1];
        if(row > 0){
            if(puzzleArray[row - 1][col] == 0){
                return true;
            }
        }
        if(row < 3){
            if(puzzleArray[row+1][col] == 0){
                return true;
            }
        }
        if(col > 0){
            if (puzzleArray[row][col-1] == 0){
                return true;
            }
        }
        if(col < 3){
            if (puzzleArray[row][col + 1]==0){
                return true;
            }
        }
        return false;
    }

    /**
     * change the value of cells
     * @param puzzleArray
     * @param position
     */
    static void Change(int puzzleArray[][],int position []){

        int [] emptyPosition = new int[2];
        emptyPosition[0] = 0;
        emptyPosition[1] = 0;

        //find the position of empty cell
        findEmptyCell(puzzleArray,emptyPosition,position);

        int row = position[0];
        int col = position[1];

        int rowEmpty = emptyPosition[0];
        int colEmpty = emptyPosition[1];

        //change the value of cells
        int temp = puzzleArray[row][col];
        puzzleArray[row][col] = puzzleArray[rowEmpty][colEmpty];
        puzzleArray[rowEmpty][colEmpty] = temp;
    }

    /**
     * find the empty cell in puzzle Array
     * @param puzzleArray
     * @param emptyPosition
     * @param position
     */
    static void findEmptyCell(int puzzleArray[][],int emptyPosition[],int position []){
        int row = position[0];
        int col = position[1];
        if(row > 0){
            if(puzzleArray[row - 1][col] == 0){
                emptyPosition[0] = row - 1;
                emptyPosition[1] = col;
            }
        }
        if(row < 3){
            if(puzzleArray[row+1][col] == 0){
                emptyPosition[0] = row + 1;
                emptyPosition[1] = col;
            }
        }
        if(col > 0){
            if (puzzleArray[row][col-1] == 0){
                emptyPosition[0] = row;
                emptyPosition[1] = col - 1;
            }
        }
        if(col < 3){
            if (puzzleArray[row][col + 1]==0){
                emptyPosition[0] = row;
                emptyPosition[1] = col + 1;
            }
        }
    }
}
